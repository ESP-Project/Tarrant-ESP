# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hef', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='event',
            index_together=set([('patient', 'name'), ('patient', 'name', 'date'), ('name', 'date')]),
        ),
    ]
