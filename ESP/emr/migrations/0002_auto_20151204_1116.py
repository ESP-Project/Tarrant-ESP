# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='encounter',
            index_together=set([('patient', 'date', 'bmi'), ('patient', 'edd'), ('patient', 'date', 'weight'), ('patient', 'date'), ('patient', 'date', 'edd'), ('patient', 'date', 'height')]),
        ),
        migrations.AlterIndexTogether(
            name='labresult',
            index_together=set([('patient', 'date', 'native_code'), ('updated_timestamp', 'native_code', 'ref_high_float', 'result_float'), ('updated_timestamp', 'native_code', 'result_string'), ('patient', 'date', 'native_code', 'result_float'), ('updated_timestamp', 'native_code', 'result_float'), ('native_code', 'result_float'), ('native_code', 'native_name'), ('native_code', 'patient', 'date', 'result_float'), ('native_code', 'ref_high_float', 'result_float'), ('native_code', 'result_string')]),
        ),
        migrations.AlterIndexTogether(
            name='prescription',
            index_together=set([('updated_timestamp', 'name')]),
        ),
    ]
