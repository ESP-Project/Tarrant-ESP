import curses                                                                
from curses import panel
import pdb, os, stat

class Menu(object):                                                          

    def __init__(self, items, stdscreen, x=0, y=0):
        self.window = stdscreen.subwin(x,y)
        self.window.keypad(1)
        self.panel = panel.new_panel(self.window)                            
        self.panel.hide()                                                    
        panel.update_panels()                                                
        self.position = 0                                                    
        self.items = self.addlastitem(items)                                                   

    def addlastitem(self, items):
        items.append(('Perform selected actions:  ','exit'))
        return items                                   

    def navigate(self, n):                                                   
        self.position += n                                                   
        if self.position < 0:                                                
            self.position = 0                                                
        elif self.position >= len(self.items):                               
            self.position = len(self.items)-1                                

    def display(self):
        self.panel.top()
        self.panel.show()
        self.window.clear()

        while True:
            self.window.refresh()
            curses.doupdate()
            for index, item in enumerate(self.items):
                if index == self.position:
                    mode = curses.A_REVERSE 
                else: 
                    mode = curses.A_NORMAL

                msg = '%d. %s' % (index, item[0])
                self.window.addstr(1+index, 1, msg, mode)

            key = self.window.getch()

            if key in [curses.KEY_ENTER, ord('\n')]: 
                #pdb.set_trace()
                if self.position == len(self.items)-1:
                    installation=Installer(self.items)
                    installation.run()
                    break
                else:
                    #these are calls to submenu, and so takes the current item list, updates, and returns.                                                        
                    self.items[self.position][1](self.items,self.items[self.position][0])
                    self.window.refresh()
                    self.panel.top()
                    self.panel.show()

            elif key == curses.KEY_UP:                                       
                self.navigate(-1)                                            

            elif key == curses.KEY_DOWN:                                     
                self.navigate(1)        

        self.window.clear()                                                  
        self.panel.hide()                                                    
        panel.update_panels()                                                
        curses.doupdate()

class Submenu(Menu):

    #redefine a couple of methods
    def addlastitem(self, items):
        items.append(('Go back','exit'))
        return items                                   

    def display(self,mainItemset,selectedMainItem):                                                       
        self.panel.top()                                                     
        self.panel.show()                                                    
        self.window.clear()                                                  

        while True:                                                          
            self.window.refresh()                                            
            curses.doupdate()                                                
            for index, item in enumerate(self.items):                        
                if index == self.position:                                   
                    mode = curses.A_REVERSE                                  
                else:                                                        
                    mode = curses.A_NORMAL                                   

                msg = '%d. %s' % (index, item[0])                            
                self.window.addstr(1+index, 1, msg, mode)                    

            key = self.window.getch()                                        

            if key in [curses.KEY_ENTER, ord('\n')]:                         
                #pdb.set_trace()
                if self.position == len(self.items)-1:                       
                    break
                else: #get the selected value and update items
                   i=0
                   for ItemTup in mainItemset:
                       if ItemTup[0]==selectedMainItem:
                           mainItemset[i]=(ItemTup[0].split(": ")[0]+': '+self.items[self.position][0],self.items)
                           break
                       i+=1
                   break

            elif key == curses.KEY_UP:                                       
                self.navigate(-1)                                            

            elif key == curses.KEY_DOWN:                                     
                self.navigate(1)        

        self.window.clear()                                                  
        self.panel.hide()                                                    
        panel.update_panels()                                                
        curses.doupdate()

class Installer(object):

    def __init__(self, items):
        self.items=items

    def run(self):
        instfile = open('./tempSetup/installplugins.txt','w')
        uninstfile = open('./tempSetup/uninstallplugins.txt','w')
        rmfile = open('./tempSetup/rmPlugins.sh','w')
        rmfile.write('#!/bin/bash\n')
        for item in self.items:
            action=item[0].split(': ')[1]
            if action=='install\n':
                plugname = item[0].split(',')[0]
                tag = item[0].split(', ')[2].split(' ')[0]
                instfile.write('-e git+https://bitbucket.org/Commoninf/esp-plugin_'+plugname+'.git@'+tag+'#egg=esp-plugin_'+plugname+'\n')
            elif action=='uninstall\n':
                #pdb.set_trace()
                plugname = item[0].split(', ')[1].split(' ')[0]
                dirname = './src/esp-plugin-'+item[0].split(',')[0] 
                uninstfile.write(plugname+'\n')
                rmfile.write('rm -rf '+dirname)
            elif action=='update\n':
                plugname = item[0].split(',')[0]
                tag = item[0].split(', ')[2].split(' ')[0]
                instfile.write('-U -e git+https://bitbucket.org/Commoninf/esp-plugin_'+plugname+'.git@'+tag+'#egg=esp-plugin_'+plugname+'\n')
        instfile.close()
        uninstfile.close()
        rmfile.close()
        st = os.stat('./tempSetup/rmPlugins.sh')
        os.chmod('./tempSetup/rmPlugins.sh', st.st_mode | stat.S_IEXEC)
        return 

class Compose(object):                                                         

    def __init__(self, stdscr):                                           
        self.screen = stdscr                                              
        curses.curs_set(0)                                                   

    menuItemDict = {}
        infile=open('./tempSetup/instUpdtPlugins.txt')
        for line in infile:
            opts=[]
            for opt in line.split('|'):
        opts.append((opt))
            descKey=opts[0]
            opts.pop(0)
        optItems=[]
        for opt in opts:
        optItems.append((opt,'break'))
            menuItemDict[descKey]=optItems
        items=[]
        submenus=[]
        i=0
        for key in sorted(menuItemDict.iterkeys()):
            default=menuItemDict[key][0][0]
            submenus.append(Submenu((list(menuItemDict[key])),self.screen,i,len(''.join(key)+': '+default)))
            items.append((''.join(key)+': '+default,submenus[i].display))
            i+=1
        main_menu = Menu(items, self.screen)                       
        main_menu.display()                                                  

if __name__ == '__main__':                                                       
    curses.wrapper(Compose)   
